<?php

namespace app\admin\controller;
use app\admin\controller\Common;
use think\Db;

class Index extends Common
{
    // 首页
    public function index(){
        $menus = Db::name('menu') -> where('status', 1) -> select();

        foreach($menus as $key => $item){
            if($item['pid'] == 0){
               $child = Db::name('menu') -> where('pid', $item['id']) -> select();
               $num = count($child);
               $menus[$key]['num'] =$num;
            }
        }
        $this -> assign('menus', $menus);
        return $this -> fetch('/index');
}

    // 退出登录
    public function sign_out(){
        Session('uname', null);
        return $this->success('成功退出！', 'admin/Login/index');
    }

    public function base(){
        return $this -> fetch('/base');
    }


}