<?php
namespace app\admin\controller\personal;

use think\Controller;
use app\admin\controller\Common;

class Basic extends Common{
    public function index(){
        return $this->fetch('/basic');
    }
}