<?php
namespace app\admin\controller;
use think\Controller;
use think\Session;
use think\Db;

class Login extends Controller{
    public function index(){
        $form = input();
//        print_r($form);
        if(empty($form)){
            return $this -> fetch('/login');
        }else{
            $users = Db::name('user') -> where('is_admin', 1) -> where('username', $form['username'])->select();
//            print_r($users);
            if(empty($users)){
                return $this->error('查无此人');
            }else{
                if(count($users) > 1){
                    foreach($users as $item){
                        if($form['password'] == $item['password']){
                            session('uname', $form['username']);
                            return $this ->success("登录成功！",'admin/Index/index');
                        }
                    }
                    return $this->error('密码错误');
                }else{
                    if($form['password'] == $users[0]['password']){
                        session('uname', $form['username']);
                        return $this ->success("登录成功！",'admin/Index/index');
                    }else{
                        return $this->error("密码错误");
                    }
                }
            }
        }

    }
}